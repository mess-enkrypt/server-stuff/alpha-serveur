// Import User Model
const UserModel = require('./models/User.js');
const MsgModel = require('./models/Msg.js');

const User = require("./user.js");
const Mailbox = require("./mailbox.js");

const Timer = require('./timer.js');

const Encryption = require('./crypt.js');

// Create a new user
var start = new Date();
User.create('Bob', 'testpublickey').then(function(value){
    console.log(Timer.display(start) + ' ' + value);

    start = Date.now();
    User.getPublicKey('Bob').then(function(value){
	console.log(Timer.display(start) + ' PublicKey of Bob: ' +  value);
    });

    start = Date.now();
    User.getPublicKey('Bob1').then(function(value){
	console.log(Timer.display(start) + ' PublicKey of Bob1: ' +  value);
    });

    start = Date.now();
    User.getUsername('testpublickey').then(function(value){
	console.log(Timer.display(start) + ' Username of testpublickey: ' +  value);
    });

    start = Date.now();
    User.getUsername('TEST').then(function(value){
	console.log(Timer.display(start) + ' Username of TEST: ' +  value);
    });

    start = Date.now();
    User.getServerEncryptionKey('Bob').then(function(value){
	console.log(Timer.display(start) + ' ServerEncryptionKey: ' + value);
    });
});

// Create a new user
start = Date.now();
User.create('John', 'testpublickeyJohn').then(function(value){
    console.log(Timer.display(start) + ' ' + value);
    // Create a new user
    start = Date.now();
    User.create('John', 'testpublickeyJohn').then(function(value){
	console.log(Timer.display(start) + ' ' + value);

	start = Date.now();
	Encryption.encrypt('Hello World ! Supinfo suce des bites', 'John').then(function(value){
	    console.log(Timer.display(start) + ' '  + 'Encrypted: ' + value);
	    start = Date.now();
	    Encryption.decrypt(value, 'John').then(function(value){
		console.log(Timer.display(start) + ' '  + 'Decrypted: ' + value);
	    });
	});
    });

});    

// Create a new user
start = Date.now();
User.create('Pablo', 'testpublickeyPablo').then(function(value){
    console.log(Timer.display(start) + ' ' + value);
    // Create a new user
    start = Date.now();
    User.delete('Pablo').then(function(value){
	console.log(Timer.display(start) + ' ' + value + 'User delete');
    });
});


start = Date.now();
Mailbox.add('Bob', 'John', "Hello World !").then(function(value){
    console.log(Timer.display(start) + ' ' +  value);
    start = Date.now();
    Mailbox.getUserMsg('John').then(function(value){
	console.log(Timer.display(start) + ' ' + value);
	start = Date.now();
	Mailbox.deleteUserMsg('John').then(function(value){
	    console.log(Timer.display(start) + ' ' + value);
	});
    });
});


// Reading Comments with username John
//UserModel.find({username: 'John'}, function(error, comments){
//    console.log(comments);
//});

// Reading Comments with publicKey testpublicKeyBob
//UserModel.find({publicKey: 'testpublicKey'}, function(error, comments){
//    console.log(comments);
//});
