const express = require('express');
const bcrypt = require('bcrypt');

const { check } = require('express-validator/check');

// Import Actions
const User = require("./actions/user.js");
const Mailbox = require("./actions/mailbox.js");

const UserModel = require("./models/User.js");

// Import Misc Actions
const Timer = require('./actions/timer.js');
const log = require('./actions/discord.js');
const Random = require("./actions/random.js");

const cache = require('./db/cache.js').client;
const subscriber = require('./db/cache.js').subscriber;

var openpgp = require('openpgp'); // use as CommonJS, AMD, ES6 module or via window.openpgp

// Import Swagger
var swaggerUi = require('swagger-ui-express');
var swaggerJSDoc = require('swagger-jsdoc');

const port = process.env.PORT || 3000;
const app = express();

// Decode body request with json
app.use(express.json());

require("./routes/user.js")(app);
require("./routes/msg.js")(app);
require("./routes/crypt.js")(app);

// SOCKET IO
const io = require('socket.io')();
const redisAdapter = require('socket.io-redis');
io.adapter(redisAdapter({ host: 'redis', port: 6379 }));

io.on("connection", (client) =>
      {
	  console.log("New Client connected on WebSocket " + client.id);
	  client.on("username", (username) => {
	      console.log("username: " + username);
	      User.isUsernameExist(username).then(value => {
		  if (value === false){
		      console.log(username + " not existing");
		      client.emit("err", "User not existing");
		  }
		  else{
		      User.getPublicKey(username).then(function(publicKey){
			  var chal = Random.hex(256);
			  openpgp.key.readArmored(publicKey).then(value =>{
			      const options = {
				  message: openpgp.message.fromText(chal),
				  publicKeys: value.keys,
			      };
			      openpgp.encrypt(options).then(function(encrypted){
				  bcrypt.hash('lol', 10, function(err, hash) {
				      UserModel.updateMany({username: username}, {$set: {challenge: hash}}, function(error, comments){
					  client.emit('auth', encrypted);
				      });
				  });
			      });
			  });
		      });
		  }
	      });
	  });
	  
	  client.on("auth", (auth, username) => {
	      UserModel.find({username: username}, function(error, comments){
		  bcrypt.compare(auth, comments[0].challenge).then(value => {
		      if (value)
		      {
			  console.log("Challenge successful");
			  client.emit("info", "Challenge successful");
			  UserModel.updateMany({username: username}, {$set: {challenge: null, connected: true, WebSocketID: client.id}}, function(error, comments){
			      subscriber.subscribe("msg/" + username);
			  });
			  
			  Mailbox.getUserMsg(username).then(function(value){
			      if (value !== "No New Msg"){
				  for(var i = 0, len = value.length; i < len; i++){
				      client.emit("msg", value[i]);
				  }
				  Mailbox.deleteUserMsg(username).then(function(value){
				      console.log("Delete " + username + "msg");
				  });
			      }
			  });
			  
		      }
		      else{
			  console.log("Challenge not successful");
			  client.emit("err", "Challenge not successful");
		      }
		  });
	      });
	  });
	  
	  client.on('disconnect', function(){
	      console.log(client.id);
	      UserModel.updateMany({WebSocketID: client.id}, {$set: {connected: false, WebSocketID: null}}, function(error, comments){
		  console.log("disconnect");
	      });
	  });

	  subscriber.on("message", function(channel, message) {
	      console.log("Message '" + message + "' on channel '" + channel + "' arrived!");
	      client.emit("msg", message);
	  });
      });

io.listen(1337);



var swaggerDefinition = {
    info: {
	title: 'Mess-Enkrypt API',
	version: '1.0.0',
	description: 'Docs to use Mess-Enkrypt API',
    },
    //host: 'localhost:3000',
    basePath: '/',
};

// options for the swagger docs
var options = {
    // import swaggerDefinitions
    swaggerDefinition: swaggerDefinition,
    // path to the API docs
    apis: ['index.js', 'routes/*.js'],// pass all in array

};

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

app.get('/', (req, res) => res.send('Messenkrypt API'));
app.get('/swagger.json', function(req, res) {   res.setHeader('Content-Type', 'application/json');   res.send(swaggerSpec); });
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.get('/health', function(req, res) {   res.setHeader('Content-Type', 'application/json');   res.send({"msg": "I'm alive"}); });


app.listen(port, () => console.log(`Messenkrypt API started\nListening on port ${port} !`));



/**
 * @swagger
 * definition:
 *   userCreate:
 *     properties:
 *       username:
 *         type: string
 *       publicKey:
 *         type: string
 *   userDestroy:
 *     properties:
 *       username:
 *         type: string
 *       destroyPassphrase:
 *         type: string
 *   username:
 *     properties:
 *       username:
 *         type: string
 *   publickey:
 *     properties:
 *       publicKey:
 *         type: string
 *   destroyPassphrase:
 *     properties:
 *       destroyPassphrase:
 *         type: string
 *   newMsg:
 *     properties:
 *       from:
 *         type: string
 *       to:
 *         type: string
 *       msg:
 *         type: string
 *         description: THE ENCRYPTED MESSAGE
 *   toencrypt:
 *     properties:
 *       toencrypt:
 *         type: string
 *         description: String sent by mobile need to be encrypt by server for remote destroy
 *       username:
 *         type: string
 *   todecrypt:
 *     properties:
 *       toencrypt:
 *         type: string
 *         description: String sent by mobile need to be decrypt by server for unlock app
 *       username:
 *         type: string
 *   responsesSuccess:
 *     userCreate:
 *       properties:
 *         message:
 *           type: string
 *           description: Server response message
 *         username:
 *           type: string
 *           description: resuming username that is created
 *         publickey:
 *           type: string
 *           description: resuming the public that is associated to username
 *         destroyPassphrase:
 *           type: string
 *           description: the passphrase to activate remote destroy
 *     getPublicKey:
 *       properties:
 *         msg:
 *           type: string
 *           description: Server response message (intended for dev debug)
 *         username:
 *           type: string
 *           description: resuming username given
 *         publicKey:
 *           type: string
 *           description: the username's public key
 *     newMsg:
 *       properties:
 *         msg:
 *           type: string
 *           description: success message
 *     getMsg:
 *       properties:
 *         msg:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               _id:
 *                 type: string
 *                 description: ID Mongo DB (Proof Message are unique)
 *               from:
 *                 type: string
 *                 description: origin username
 *               to:
 *                 type: string
 *                 description: destination
 *               msg:
 *                 type: string
 *                 description: THE ENCRYPTED MESSAGE
 *               date:
 *                 type: string
 *                 description: date (format YYYY-MM-DDTHH:mm:ss.???)
 *   responsesFail:
 *     userCreate:
 *       properties:
 *         err:
 *           type: string
 *           description: telling if username or publickey is already existing
 *     getPublicKey:
 *       properties:
 *             err:
 *               type: string
 *               description: the error message
 *     newMsg:
 *       properties:
 *         msg:
 *           type: string
 *           description: error message
 *     getMsg:
 *       properties:
 *         msg:
 *           type: string
 *           description: server response (\"No New Msg or User not existing\")
 *     noUsernameFound:
 *       properties:
 *         err:
 *           type: string
 *           description: server response (\"Username not found\")
 */
