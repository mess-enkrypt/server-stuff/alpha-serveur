// Import Node Modules
const { check, validationResult } = require('express-validator/check');
const express = require('express');
const bcrypt = require('bcrypt');

// Import Actions
const User = require("../actions/user.js");
const Crypt = require("../actions/crypt.js");
// Import Misc Actions
const Timer = require('../actions/timer.js');
const log = require('../actions/discord.js');

var start = new Date();

const encryptRoute = '/encrypt';
const decryptRoute = '/decrypt';

module.exports = function(app){

    // MESSAGE ROUTES
    
    /**
     * @swagger
     * /encrypt:
     *   post:
     *     tags:
     *       - encryption
     *     description: Crypt string sends by user and generate serverEncryptionKey
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: toencrypt
     *         description: Key generate by mobile use to encrypt everything in app to lock app
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/toencrypt'
     *     responses:
     *       200:
     *         description: new string to keep to send to /decrypt when you want to decrypt (for remote destroy)
     *         schema:
     *       404:
     *         description: Username not found
     *         schema:
     *           $ref: '#/definitions/responsesFail/noUsernameFound'
     */
    app.post(encryptRoute, [ check('username').exists(),
			     check('toencrypt').exists(),
			     check('username', 'Username not existing').custom(username => { return User.isUsernameExist(username).then(value => { return value; }); }),
			     check('username', 'User not active').custom(username => { return User.isActive(username).then(value => { return value; }); })], function(req, res) {
				 start = Date.now();
				 const errors = validationResult(req);
				 if (!errors.isEmpty()) {
				     log.write(Timer.display(start) + ' ' + encryptRoute + ' ' + JSON.stringify(errors.array(), null, 4));
				     return res.status(422).json({ errors: errors.array() });
				 }
				 Crypt.encrypt(req.body.toencrypt, req.body.username).then(function(encrypted){
				     res.json({
					 msg: encrypted
				     });
				     log.write(Timer.display(start) + encryptRoute + ' ' + encrypted);
				 });
			     });
    
    /**
     * @swagger
     * /decrypt:
     *   post:
     *     tags:
     *       - encryption
     *     description: Decrypt string send to server with serverEncryptionKey
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: todecrypt
     *         description: string sent back by route to encrypt
     *         in: body
     *         required: true    
     *         schema:
     *           $ref: '#/definitions/todecrypt'
     *     responses:
     *       200:
     *         description: msg
     *         schema:
     *       404:
     *         description: Username not found
     *         schema:
     *           $ref: '#/definitions/responsesFail/noUsernameFound'
     */
    app.post(decryptRoute, [ check('todecrypt').exists(),
			     check('username').exists(),
			     check('username', 'Username not existing').custom(username => { return User.isUsernameExist(username).then(value => { return value; }); }),
			     check('username', 'User not active').custom(username => { return User.isActive(username).then(value => { return value; }); })], function(req, res) {
				 start = Date.now();
				 const errors = validationResult(req);
				 if (!errors.isEmpty()) {
				     log.write(Timer.display(start) + ' ' + decryptRoute + ' ' + JSON.stringify(errors.array(), null, 4));
				     return res.status(422).json({ errors: errors.array() });
				 }
				 Crypt.decrypt(req.body.todecrypt, req.body.username).then(function(decrypted){
				     res.json({
					 msg: decrypted
				     });
				     log.write(Timer.display(start) + decryptRoute + ' ' + decrypted);
				 });
			     });
}
