// Import Node Modules
const { check, validationResult } = require('express-validator/check');
const express = require('express');
const bcrypt = require('bcrypt');

// Import Actions
const Mailbox = require("../actions/mailbox.js");
const User = require("../actions/user.js");

// Import Misc Actions
const Timer = require('../actions/timer.js');
const log = require('../actions/discord.js');

var start = new Date();

const msgSendRoute = '/msg/send';
const msgReceiveRoute = '/msg/receive';

module.exports = function(app){

    // MESSAGE ROUTES
    
    /**
     * @swagger
     * /msg/send:
     *   post:
     *     tags:
     *       - msg
     *     description: Send a new message
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: message
     *         description: new message
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/newMsg'
     *     responses:
     *       200:
     *         description: msg
     *         schema:
     *           $ref: '#/definitions/responsesSuccess/newMsg'
     *       422:
     *         description: Username not found
     *         schema:
     *           $ref: '#/definitions/responsesFail/noUsernameFound'
     */
    app.post(msgSendRoute, [ check('from').exists(),
			     check('to').exists(),
			     check('msg').exists(),
			     check('from', 'Username not existing').custom(username => { return User.isUsernameExist(username).then(value => { return value; }); }),
			     check('to', 'Username not existing').custom(username => { return User.isUsernameExist(username).then(value => { return value; }); }),
			     check('from', 'User not active').custom(username => { return User.isActive(username).then(value => { return value; }); }),
			     check('to', 'User not active').custom(username => { return User.isActive(username).then(value => { return value; }); })], function(req, res) {
				 start = Date.now();
				 const errors = validationResult(req);
				 if (!errors.isEmpty()) {
				     log.write(Timer.display(start) + msgSendRoute + ' ' + JSON.stringify(errors.array(), null, 4));
				     return res.status(422).json({ errors: errors.array() });
				 }
				 Mailbox.add(req.body.from, req.body.to, req.body.msg).then(function(value){
				     res.json({
					 msg: value
				     });
				     log.write(Timer.display(start) + msgSendRoute + ' ' + value);
				 });
			     });

    /**
     * @swagger
     * /msg/receive:
     *   post:
     *     tags:
     *       - msg
     *     description: Ask server if new message available to get
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: username
     *         description: username
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/username'
     *     responses:
     *       200:
     *         description: msg
     *         schema:
     *           $ref: '#/definitions/responsesSuccess/getMsg'
     *       418:
     *         description: No new msg
     *         schema:
     *           $ref: '#/definitions/responsesFail/getMsg'
     *       422:
     *         description: Username not found
     *         schema:
     *           $ref: '#/definitions/responsesFail/noUsernameFound'
     */
    app.post(msgReceiveRoute, [ check('username').exists(),
				check('username', 'Username not existing').custom(username => { return User.isUsernameExist(username).then(value => { return value; });}),
				check('username', 'No new msg').custom(username => { return Mailbox.isUserNewMsg(username).then(value => { return value; }); }),
				check('username', 'User not active').custom(username => { return User.isActive(username).then(value => { return value; }); })], function(req, res) {
				    start = Date.now();
				    const errors = validationResult(req);
				    if (!errors.isEmpty()) {
					if (errors.array()[0].msg === "No new msg")
					    return res.status(418).json({ errors: errors.array()  });
					else{
					    log.write(Timer.display(start) + ' ' + msgReceiveRoute + ' ' + JSON.stringify(errors.array(), null, 4));
					    return res.status(422).json({ errors: errors.array() });
					}
				    }
				    Mailbox.getUserMsg(req.body.username).then(function(value){
					res.json({
					    msg: value
					});
					log.write(Timer.display(start) + msgReceiveRoute + ' ' + req.body.username);
					start = Date.now();
					Mailbox.deleteUserMsg(req.body.username).then(function(value){
					    log.write(Timer.display(start) + msgReceiveRoute + ' ' + value);
					});
				    });
				});
}
