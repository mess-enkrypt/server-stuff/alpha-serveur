// Import Node Modules
const { check, validationResult } = require('express-validator/check');
const express = require('express');
const bcrypt = require('bcrypt');

// Import Actions
const User = require("../actions/user.js");

// Import Misc Actions
const Timer = require('../actions/timer.js');
const log = require('../actions/discord.js');

var start = new Date();

/* ROUTES */
const registerRoute = '/user/register';
const getUsernameRoute = '/user/get/username';
const getPublicKeyRoute = '/user/get/publickey';
const destroyRoute = '/user/destroy';

module.exports = function(app){
    /**
     * @swagger
     * /user/register:
     *   post:
     *     tags:
     *       - user
     *     description: Creates a new user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: user
     *         description: user to create
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/userCreate'
     *     responses:
     *       201:
     *         description: User created
     *         schema:
     *           $ref: '#/definitions/responsesSuccess/userCreate'
     *       422:
     *         description: Username or Publickey already existing
     *         schema:
     *           $ref: '#/definitions/responsesFail/userCreate'
     */
    app.post(registerRoute, [ check('username').exists(),
			      check('publicKey').exists(),
			      check('username', 'Username already exiting').custom(username => { return User.isUsernameExist(username).then(value => { return !value; }); }),
			      check('publicKey', 'PublicKey already existing').custom(publicKey => { return User.isPubKeyExist(publicKey).then(value => { return !value; }); })], function(req, res) {
				  start = Date.now();
				  const errors = validationResult(req);
				  if (!errors.isEmpty()) {
				      log.write(Timer.display(start) + registerRoute + ' ' + JSON.stringify(errors.array(), null, 4));
				      return res.status(422).json({ errors: errors.array() });
				  }
				  log.write(registerRoute + ' ' + "Creating User: " + JSON.stringify(req.body, null, 4));
				  User.create(req.body.username, req.body.publicKey).then(function(value){
				      res.status(201).json(value);
				      log.write(Timer.display(start) + registerRoute + ' ' + value.username + ' Created');
				  }).catch(function(CreateErr){
				      console.log(CreateErr.message);
				      res.status(400).json({ "err": CreateErr.message});
				      log.write(Timer.display(start) + registerRoute + ' ' + req.body.username + " " + CreateErr.message);
				      log.write(Timer.display(start) + registerRoute + ' ' + req.body.username + ' not Created');
				  });
			      });
    
    /**
     * @swagger
     * /user/get/username:
     *   post:
     *     tags:
     *       - user
     *     description: Get username from public key
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: publicKey
     *         description: publicKey of user you want to find.
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/publicKey'
     *     responses:
     *       200:
     *         description: username
     *       422:
     *         description: publicKey not found
     */
    app.post(getUsernameRoute, [ check('publicKey').exists(),
				 check('publicKey', 'PublicKey not found').custom(publicKey => { return User.isPubKeyExist(publicKey).then(value => { return value; }); })], function(req, res) {
				     start = Date.now();
				     const errors = validationResult(req);
				     if (!errors.isEmpty()) {
					 log.write(Timer.display(start) + getUsernameRoute + ' ' + JSON.stringify(errors.array(), null, 4));
					 return res.status(422).json({ errors: errors.array() });
				     }
				     User.getUsername(req.body.publicKey).then(function(value){
					 var user = {
					     msg: "GetUsername Successful",
					     username: value,
					     publicKey: req.body.publicKey
					 };
					 res.json(user);
					 log.write(Timer.display(start) + getUsernameRoute + ' ' + "Username: " + value);
				     });
				 });
    
    /**
     * @swagger
     * /user/get/publickey:
     *   post:
     *     tags:
     *       - user
     *     description: Get public key from username
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: username
     *         description: username of user you want to find.
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/username'
     *     responses:
     *       200:
     *         description: publickey
     *         schema:
     *           $ref: '#/definitions/responsesSuccess/getPublicKey'
     *       422:
     *         description: Username not found
     *         schema:
     *           $ref: '#/definitions/responsesFail/getPublicKey'
     */
    app.post(getPublicKeyRoute, [ check('username').exists(),
				  check('username', 'Username not found').custom(username => { return User.isUsernameExist(username).then(value => { return value; }); }),
				  check('username', 'User not active').custom(username => { return User.isActive(username).then(value => { return value; }); })], function(req, res) {
				      start = Date.now();
				      const errors = validationResult(req);
				      if (!errors.isEmpty()) {
					  log.write(Timer.display(start) + getPublicKeyRoute + ' ' + JSON.stringify(errors.array(), null, 4));
					  return res.status(422).json({ errors: errors.array() });
				      }
				      User.getPublicKey(req.body.username).then(function(value){
					  var user = {
					      msg: "GetPublicKey Succesful",
					      username: req.body.username,
					      publicKey: value
					  };
					  res.json(user);
					  log.write(Timer.display(start) + getPublicKeyRoute + ' ' + req.body.username);
				      });	
				  });
    
    /**
     * @swagger
     * /user/destroy:
     *   delete:
     *     tags:
     *       - user
     *     description: Destroy user's serverEncryptionKey in server
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: user
     *         description: user to remote destroy
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/userDestroy'
     *     responses:
     *       200:
     *         description: User deleted
     *       422:
     *         description: Wrong Passphrase or Username not found
     *
     */
    app.delete(destroyRoute, [ check('username').exists(),
			       check('destroyPassphrase').exists(),
			       check('username', 'Username not existing').custom(username => { return User.isUsernameExist(username).then(value => { return value; }); }),
			       check('destroyPassphrase', 'Wrong Passphrase').custom((destroyPassphrase, {req}) => { return User.getDestroyPassphrase(req.body.username).then(value => { return bcrypt.compare(destroyPassphrase, value); }); }),
			       check('username', 'User not active').custom(username => { return User.isActive(username).then(value => { return value; }); })], function(req, res) {
				   start = Date.now();
				   const errors = validationResult(req);
				   if (!errors.isEmpty()) {
				       log.write(Timer.display(start) + destroyRoute + ' ' + JSON.stringify(errors.array(), null, 4));
				       return res.status(422).json({ errors: errors.array() });
				   }
				   User.delete(req.body.username).then(function(value){
				       res.json({
					   msg: value
				       });
				       log.write(Timer.display(start) + destroyRoute + ' ' + req.body.username + ' ' + value);
				   });
			       });
}
