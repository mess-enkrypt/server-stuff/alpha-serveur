var db = require('../db/db.js')
var User = db.model('User', {
    username: { type: String, required: true },
    publicKey:     { type: String, required: true },
    serverEncryptionKey:     { type: String, required: false },
    destroyPassphrase: { type: String, required: true },
    date:     { type: Date, required: true, default: Date.now },
    active: { type: Boolean, required: true, default: true },
    challenge: { type: String, required: false },
    connected: { type: Boolean, required: false },
    WebSocketID : { type: String, required: false }
})
module.exports = User
