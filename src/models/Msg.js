var db = require('../db/db.js')
var Msg = db.model('Msg', {
  from: { type: String, required: true },
  to:   { type: String, required: true },
  msg:  { type: String, required: true },
  date: { type: Date, required: true, default: Date.now }
})
module.exports = Msg
