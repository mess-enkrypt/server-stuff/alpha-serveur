// Import the discord.js module
const Discord = require('discord.js');

const DISCORD_LOG = process.env.DISCORD_LOG || false;

if (DISCORD_LOG != false && process.env.DISCORD_TOKEN === undefined || process.env.DISCORD_KEY === undefined)
    console.log("Douchebag, you need to give DISCORD_TOKEN && DISCORD_API if you want to write on discord");

// Create a new webhook
const hook = new Discord.WebhookClient(process.env.DISCORD_TOKEN, process.env.DISCORD_KEY);

module.exports = {
    write: function(value){
	if (DISCORD_LOG !== false && process.env.DISCORD_TOKEN != undefined && process.env.DISCORD_KEY != undefined)
	    hook.send("```" + value + "```");
	console.log(value);
    }
}
// Send a message using the webhook
//hook.send('I am now alive!');
