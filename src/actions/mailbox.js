// Import User Model
const UserModel = require('../models/User.js');
const MsgModel = require('../models/Msg.js');

const cache = require('../db/cache.js').client;

module.exports = {

    add: function (from, to, message) {
	return new Promise( function(resolve, reject) {	
	    console.log("Creating Msg: { from: " + from + " to: " + to + ' msg: ' + message + " }"); 
	    UserModel.find({username: from}, function(error, comments){
		if (comments[0] === undefined)
		    reject('Username Sender ' + from + ' not existing');
		else {
		    UserModel.find({username: to}, function(error, comments){
			if (comments[0] === undefined)
			    reject('Username Recipient ' + to + ' not existing');
			else {
			    var msg = new MsgModel({ from: from, to: to, msg: message });
			    if (comments[0].connected === true){
				// PUBLISH MSG ON CHANNEL ON REDIS
				cache.publish("msg/" + to, msg.toString(), function(){
				    console.log("MSG Publish to Redis");
				    resolve(msg);
				});
			    }
			    else{
				msg.save().then(value => {
				    console.log("MSG Publish to MongoDB");
				    resolve(value);
				});
			    }   
			}
		    });
		}
	    });
	    
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    deleteUserMsg: function (username){
	return new Promise( function(resolve, reject) {
	    MsgModel.find({to: username}, function(error, comments){
		if (comments[0] === undefined)
		    reject('Already Delete or User not existing');
		else {
		    MsgModel.deleteMany({to: username}, function(error, comments){
			resolve('Delete ' + username + "'s Messages");
		    });
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
	    },
    getUserMsg: function (username){
	return new Promise( function(resolve, reject) {
	    MsgModel.find({to: username}, {"_id": 1, "from": 1, "to": 1, "msg": 1, "date": 1}, function(error, comments){
		if (comments[0] === undefined)
		    reject('No New Msg');
		else 
		    resolve(comments);
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    isUserNewMsg: function (username){
	return new Promise( function(resolve, reject) {
	    MsgModel.find({to: username}, {"_id": 1, "from": 1, "to": 1, "msg": 1, "date": 1}, function(error, comments){
		if (comments[0] === undefined)
		    resolve(false);
		else 
		    resolve(true);
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },    
};
