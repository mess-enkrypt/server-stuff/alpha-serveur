// Import Crypto native node module
const crypto = require('crypto');
// Import User Model
const UserModel = require('../models/User.js');
const Random = require('./random.js');


var algorithm = 'aes256'; // or any other algorithm supported by OpenSSL

module.exports = {

    encrypt: function (tocrypt, username) {
	return new Promise( function(resolve, reject) {	
	    console.log("Crypt: { from: " + username + " string: " + tocrypt + " }");
	    UserModel.find({username: username}, function(error, comments){
		if (comments[0] === undefined)
		    reject('Username ' + username + ' not existing');
		else {
		    var key = Random.hex(512);

		    var cipher = crypto.createCipher(algorithm, key);
		    var encrypted = cipher.update(tocrypt, 'utf8', 'hex') + cipher.final('hex');

		    UserModel.findOneAndUpdate({username: username}, {$set: {serverEncryptionKey: key}}, function(error, comments){
			resolve(encrypted);
		    });
		    
		}
	    });
	    
	}).catch(function(err) {
	    console.log(err);
	    return(err);
	})
	    },
    decrypt: function (todecrypt, username){
	return new Promise( function(resolve, reject) {
	    UserModel.find({username: username}, function(error, comments){
		if (comments[0] === undefined)
		    reject('Username ' + username + ' not existing');
		else {
		    var decipher = crypto.createDecipher(algorithm, comments[0].serverEncryptionKey);
		    var decrypted = decipher.update(todecrypt, 'hex', 'utf8') + decipher.final('utf8');

		    resolve(decrypted.toString());
		}
	    });
	}).catch(function(err) {
	    console.log(err);
	    return(err);
	})
	    }
};
