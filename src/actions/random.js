var crypto = require('crypto')

module.exports = {
    
    hex: function (len) {
	return crypto
            .randomBytes(Math.ceil(len / 2))
            .toString('hex') // convert to hexadecimal format
            .slice(0, len) // return required number of characters
    },
    num: function (len) {
	return parseInt(crypto
			.randomBytes(Math.ceil(len / 2))
			.toString('hex') // convert to hexadecimal format
			.slice(0, len), 16);
    }

};
