
module.exports = {

    setTimeout: function (start) {
	// execution time simulated with setTimeout function
	var end = new Date() - start;

	return end;
    },

    display: function(start) {
	return '[' + this.setTimeout(start) + 'ms] ';
    }

};
