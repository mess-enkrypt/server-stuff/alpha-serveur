// Import Node Modules
const bip39 = require('bip39');
const bcrypt = require('bcrypt');

// Import User Model
const UserModel = require('../models/User.js');

const cache = require('../db/cache.js').client;

// Import Random function from random.js
const Random = require("./random.js");

var openpgp = require('openpgp'); // use as CommonJS, AMD, ES6 module or via window.openpgp

module.exports = {

    create: async function (username, publicKey, serverEncryptionKey) {
	return new Promise( function(resolve, reject) {	
	    UserModel.find({username: username}, function(error, comments){
		if (comments[0] !== undefined){
		    reject('Username ' + username + ' already existing');
		}
		else {
		    UserModel.find({publicKey: publicKey}, function(error, comments){
			if (comments[0] !== undefined)
			    reject('publicKey ' + publicKey + ' already existing');
			else {
			    serverEncryptionKey = serverEncryptionKey || null
			    var destroyPassphrase = bip39.generateMnemonic();
			    username = username + '#' + Random.num(5);
			    bcrypt.hash(destroyPassphrase, 10, function(err, hash) {
				openpgp.key.readArmored(publicKey).then(value =>{
				    const options = {
					message: openpgp.message.fromText(destroyPassphrase),
					publicKeys: value.keys,
				    };
				    openpgp.encrypt(options).then(function(encrypted){
					if (encrypted === undefined)
					    reject("PGP Key Error");
					else{
					    var user = new UserModel({ username: username, publicKey: publicKey, serverEncryptionKey: serverEncryptionKey, destroyPassphrase: hash, active: true});
					    user.save().then(() => resolve({
						message: "User created",
						username: username,
						publicKey: publicKey,
						destroyPassphrase: destroyPassphrase //encrypted.data
					    }));
					    }
				    }).catch(function(PGPerr){
					reject(PGPerr);
				    });
				});
			    });
			}
		    });
		}
	    });
	    
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    getPublicKey: function (username){
	return new Promise( function(resolve, reject) {
	    cache.get(username, (err, result) => {
		result = JSON.parse(result);
		if (result !== undefined && result !== null){
		    resolve(result.publicKey);
		}
		else{
		    UserModel.find({username: username}, function(error, comments){
			if (comments[0] === undefined){
			    console.log(error);
			    reject('Username ' + username +  ' not existing');
			}
			else{
			    cache.set(username, JSON.stringify(comments[0]));
			    resolve(comments[0].publicKey);
			}
		    });
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    getUsername: function (publicKey){
	return new Promise( function(resolve, reject) {
	    UserModel.find({publicKey: publicKey}, function(error, comments){
		if (comments[0] === undefined)
		    reject('publicKey ' + publicKey +  ' not existing');
		else {
		    resolve(comments[0].username);
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    getServerEncryptionKey: function (username){
	return new Promise( function(resolve, reject) {
	    UserModel.find({username: username}, function(error, comments){
		if (comments[0] === undefined)
		    reject('username ' + username +  ' not existing');
		else {
	 	    resolve(comments[0].serverEncryptionKey);
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    getDestroyPassphrase: function(username){
	return new Promise( function(resolve, reject) {
	    UserModel.find({username: username}, function(error, comments){
		if (comments[0] === undefined)
		    reject('username ' + username +  ' not existing');
		else {
		    resolve(comments[0].destroyPassphrase);
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	})
    },
    delete: function (username){
	return new Promise( function(resolve, reject) {
	    UserModel.find({username: username}, function(error, comments){
		if (comments[0] === undefined)
		    reject('Username' + username + ' not existing');
		else {
		    //UserModel.deleteOne({username: username}, function(error, comments){
		    //	resolve("Delete");
		    //  })
		    UserModel.updateMany({username: username}, {$set: {serverEncryptionKey: 'NULL', active: false}}, function(error, comments){
			resolve("Delete");
		    });	    
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    isUsernameExist: function (username){
	return new Promise( function(resolve, reject) {
	    cache.get(username, (err, result) => {
                result = JSON.parse(result);
		if (result !== undefined && result !== null){
		    resolve(true);
		}
		else{
		    UserModel.find({username: username}, function(error, comments){
			if (comments[0] !== undefined){
			    cache.set(username, JSON.stringify(comments[0]));
			    resolve(true);
			}
			else {
			    resolve(false);
			}
	 	    });
		}
	    });
	});
    },
    isPubKeyExist: function (publicKey){
	return new Promise( function(resolve, reject) {
	    UserModel.find({publicKey: publicKey}, function(error, comments){
		if (comments[0] !== undefined)
		    resolve(true);
		else {
		    resolve(false);
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    isActive: function (username){
	return new Promise( function(resolve, reject) {
	    cache.get(username, (err, result) => {
                result = JSON.parse(result);
		if (result !== undefined && result !== null){
		    resolve(result.active);
		}
		else{
		    UserModel.find({username: username}, {'active': 1}, function(error, comments){
			if (comments[0] === undefined)
			    reject('No Username');
			else {
			    cache.set(username, JSON.stringify(comments[0]));
			    resolve(comments[0].active);
			}
		    });
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    }
};
